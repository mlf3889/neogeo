
// NeoGeo Watch NeoPixel Ring 
// Simulates Tony Stark "Reactor"
// Author - Marc Funston  marc.funston@gmail.com
// released under the GPLv3 license to match the rest of the AdaFruit NeoPixel library

#include <Adafruit_NeoPixel.h>

#define PIN            6 // NeoGeo pin controlling NeoPixel
#define NUMPIXELS      16 // Number of NeoPixels

// Set NeoPixel color and the NeoPixel library.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
int r = 0;
int g = 0;
int b = 150;

void setup() {
pixels.begin(); 
}

void loop() {

 tonyStark(r, g, b, 100);

}

void tonyStark(int r, int g, int b, int t){
    for (int i = 0; i < NUMPIXELS; i++) {
       pixels.setPixelColor((i + 1) % NUMPIXELS, pixels.Color(int(r / 80), int(g / 80), int(b / 80)));
       pixels.setPixelColor((i + 2) % NUMPIXELS, pixels.Color(int(r / 33), int(g / 33), int(b / 33)));
       pixels.setPixelColor((i + 3) % NUMPIXELS, pixels.Color(int(r / 5), int(g / 5), int(b / 5)));
       pixels.setPixelColor((i + 4) % NUMPIXELS, pixels.Color(r, g, b));
       pixels.show(); // execute settings
       delay(t); // longer = slower rotation
       clearDisplay();
    }
}

void clearDisplay(){
  for (int i = 0; i < NUMPIXELS; i++) {
    pixels.setPixelColor(i, pixels.Color(0, 0, 0));
  }
  pixels.show(); // execute settings
}
